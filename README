qa.debian.org
=============

This is where the data for the Debian Quality Assurance lives.

Users and Groups
----------------

There is a 'qa' unix group which should give you access to pretty much every QA
file. Some exceptions are mboxes, where exim insists on removing the g+w bit.
The usual bits about using the right umask apply. Ask a QA member if you want
to join.

Membership to the 'qa' unix group is automatically mirrored in alioth's
'qa' project. The group there is required for write access to Git.

Some hosts have a 'qa' user. Access via sudo is available on request. Please
ask a QA member to second your request before asking debian-admin. The access
is per host, so please indicate which hosts you want. The ~qa home should not
be used for anything but ssh keys (adding yours is probably a good idea, so you
don't have to expose your password all the time).

Files
-----

Data is stored in Git:
  https://salsa.debian.org/qa/qa

To get commit notifications, subscribe to the 'qa.debian.org' package in the
package tracker and add the 'vcs' keyword. Either by email:
  ( echo "subscribe qa.debian.org"
    echo "keyword qa.debian.org + vcs" ) | mail control@tracker.debian.org
Or through the web (use the subscribe button, then tweak the subscription
details in the drop-down):
https://tracker.debian.org/pkg/qa.debian.org

All newly added stuff should go into the 'data' subdir (see below), only
private data (like the MIA database) should be outside of 'data'.

Hosts
-----

quantz: qa.debian.org lives here, the above-mentioned Git tree being checked
out to /srv/qa.debian.org/. The 'data' subdir is http-exported to
https://qa.debian.org/data/. quantz has a 'qa' user.

ticharich: hosts tracker.debian.org, /srv/tracker.debian.org hosts
everything related to the service. The services runs as the 'qa' user.

franck: hosts ftp-master and a cronjob to watch the accepted (aka incoming) and
new queues in ~jeroen/public_html/.

alioth: running dehs in /home/groups/dehs/.

Resources
---------

https://qa.debian.org/
https://bugs.debian.org/qa.debian.org
https://wiki.debian.org/qa.debian.org

Dependencies
------------

Packages installed for qa@quantz:

db4.8-util gnuplot-nox libapache2-mod-fcgid libapt-pkg-perl libdbd-pg-perl
libdate-manip-perl libdpkg-perl libmailtools-perl libsoap-lite-perl
libtemplate-perl libxml-simple-perl php5 php5-cgi php5-cli php5-ldap php5-pgsql
python-apt python-psycopg2 wml libdbd-pg-ruby libmail-sendmail-perl
libdate-calc-perl libnet-ldap-perl

 -- Christoph Berg <myon@debian.org>  Sun, 23 Jul 2006 21:55:41 +0200
