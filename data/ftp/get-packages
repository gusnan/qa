#!/usr/bin/perl -w

# Output the packages file for a given architecute and suite
# Copyright (C) 2005 Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
#
# This program is free software.  It is distributed under the terms of
# the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at https://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
# MA 02111-1307, USA.

use strict;
use Getopt::Long;

my $ROOT = "/srv/qa.debian.org/ftp";

sub help {
    my $err = shift;
    my $text = <<"EOT";
Usage: get-packages [-d] [-v archive] -s suite [-c compontent] -a arch [-i]
Output the packages file for a given architecute and suite

Options:
 -v, --archive <ar>  filter on archive: debian{,-security,-backports,-debug}
                     (default: {debian,-security})
 -s, --suite <s>     select suite: sid, testing, ...
 -c, --component <c> filter on component: main, non-US/contrib, ... (default: all)
 -a, --arch <a>      select architecture (i386, sparc, source, ...)
 -i, --installer     select d-i udebs instead of debs

 -d, --debug         print displayed package lists instead
 -h, --help          show this help message
EOT

    print STDERR "$err\n" if $err;
    print STDERR $text;
    exit 1;
}

my $opt_archive = '*';
my $opt_suite = undef;
my $opt_component = '*';
my $opt_arch = undef;
my $opt_installer = 0;
my $opt_debug = 0;
my $opt_help = 0;

my @ARGV_SAVE = @ARGV;

# process commandline options
GetOptions("archive|v=s" => \$opt_archive,
           "suite|s=s" => \$opt_suite,
           "component|c=s" => \$opt_component,
           "arch|a=s" => \$opt_arch,
           "installer|i" => \$opt_installer,
           "debug|d" => \$opt_debug,
           "help|h" => \$opt_help)
    or die("error parsing options\n");

sub go {
    my $opt = shift;
    my $ext = 'xz';
    my @globs = glob("${opt}.xz");
    if (not @globs or not -e $globs[0]) {
        $ext = 'gz';
        @globs = glob("${opt}.gz");
    }
    if ($opt_debug) {
        print "$opt\n ";
        print "$ext\n ";
        print join "\n ", @globs;
    }
    if (not @globs or not -e $globs[0]) {
        # Not a single matching list
        help("No matches found: @ARGV_SAVE");
    }
    exit 0 if $opt_debug;
    if ("$ext" eq "xz") {
        exec "xzcat @globs";
    } elsif ("$ext" eq "gz") {
        exec "zcat @globs";
    } else {
        die("Unrecognized extension: $ext");
    }
}

help("") if $opt_help;
help("Please specify a compontent") unless defined $opt_component;
help("Please specify an architecture") unless defined $opt_arch;

$opt_archive = 'debian{,-security}' if ($opt_archive eq '*');
$opt_suite = "$opt_suite-backports" if ($opt_suite and $opt_archive eq 'debian-backports');

my $path = "$ROOT/$opt_archive/dists/$opt_suite/$opt_component";

if ($opt_arch eq 'source') {
    go("$path/source/Sources");
} elsif ($opt_installer) {
    go("$path/debian-installer/binary-$opt_arch/Packages");
} else {
    go("$path/binary-$opt_arch/Packages");
}

# vim: sw=4 et
