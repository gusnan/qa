This script is run on master.d.o in igenibel's crontab each hour
12 * * * * ($HOME/bin/process-index.pl /srv/bugs.debian.org/spool/index.db /srv/bugs.debian.org/etc/indices/sources > $HOME/public_html/bugs.txt)

It does not count the merged bugs twice.
